//Initialisation AJAX

        let httpRequest;
        //Lier la requête à la validation du formulaire
        document.getElementById("buttonStats").addEventListener('click', verif);

        function verif () {
        let nom = document.getElementById('Nom').value;
        let prenom = document.getElementById('Prenom').value;
        let pattern = 0;
        const re = new RegExp("[a-zA-Z/-]");
            if(re.test(nom) && re.test(prenom)){
            console.log("c ok");
            makeRequest()
            }
        }

        function makeRequest() {
   
            //Initialiser une requête
            httpRequest = new XMLHttpRequest();
            //Message d'erreur
            if (!httpRequest) {
                alert('Abandon ! Impossible de créer une instance de XMLHTTP');
                return false;
            }
            //Récupérer l'input battletag et remplacer '#' par '-'
            const regex = /#/;
            let battletag = document.getElementById('battletag').value;
            let battletag2 = battletag.replace(regex, "-");
    
            //Se connecter à l'API Overwatch via le battletag - ouvrir une requête http
            httpRequest.open('GET', 'https://ow-api.com/v1/stats/pc/eu/' + battletag2 + '/profile');
            if (!battletag) {
                displayClean();
                alert("Veuillez rentrer un battletag");
    
            } else {
                
                httpRequest.send()
                //displayStats = fonction qu'on veut exécuter au chargement de la requête
                httpRequest.onload = function () {
                    displayStats()
                }
            }
        }
    
        function displayStats() {
    
            let userStats = JSON.parse(httpRequest.response); 
            if (httpRequest.status == 404 || httpRequest.status == 400 ){
                displayClean();
                return alert("T'es qu'un grosse merde go jouer péon ou rentre un battletag valide"); 
            } else {
            //Afficher les parties gagnées
            let showGamesWon = document.getElementById('gamesWon');
            showGamesWon.textContent = userStats.competitiveStats.games.won;
            //Afficher le classement
            let showRating = document.getElementById('rating');
            showRating.textContent = userStats.rating;
            //Afficher les parties jouées
            let showGamesPlayed = document.getElementById('gamesPlayed');
            showGamesPlayed.textContent = userStats.competitiveStats.games.played;
            //Afficher les défaites
            let showDefeats = document.getElementById('defeats');
            showDefeats.textContent = userStats.competitiveStats.games.played - userStats.competitiveStats.games.won;
        }
        function displayClean() {

            //Clean parties gagnées
            let showGamesWon = document.getElementById('gamesWon');
            showGamesWon.textContent = "";
            //Clean classement
            let showRating = document.getElementById('rating');
            showRating.textContent = "";
            //Clean parties jouées
            let showGamesPlayed = document.getElementById('gamesPlayed');
            showGamesPlayed.textContent = "";
            //Clean défaites
            let showDefeats = document.getElementById('defeats');
            showDefeats.textContent = "";
        };
   
        }
        
       

